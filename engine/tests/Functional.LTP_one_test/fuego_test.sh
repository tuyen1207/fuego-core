one_test=$FUNCTIONAL_LTP_ONE_TEST_TEST

# Don't allow jobs to share build directories
# the "test_successfully_built" flag for one spec
# is not accurate for another spec, which has a different
# individual program to deploy.
FUNCTIONAL_LTP_ONE_TEST_PER_JOB_BUILD="true"

# This test can either run the test directly, or run it via
# runltp and ltp-pan.  To use runltp, set the value of "scenario"
# in the spec to the correct scenario name.  (e.g. 'syscalls')
# The scenario is the name of the the file in the runtest directory
# that has an entry (possibly with command line arguments) for this test.

function test_pre_check {
    assert_define FUNCTIONAL_LTP_ONE_TEST_TEST
}

function test_build {
    # check for LTP build directory
    LTP_BUILD_DIR="${WORKSPACE}/$(echo $JOB_BUILD_DIR | sed s/LTP_one_test/LTP/ | sed s/$TESTSPEC/default/)"
    echo "LTP_BUILD_DIR=${LTP_BUILD_DIR}"

    # if not already built, build LTP
    if [ ! -e ${LTP_BUILD_DIR}/fuego_test_successfully_built ] ; then
        echo "Building parent LTP test..."
        ftc run-test -b $NODE_NAME -t Functional.LTP -p b
        # NOTE: vars used in ftc run-test should not leak into this environment
        # that is, none of our test vars should have changed.
    fi

    cp ${LTP_BUILD_DIR}/target_bin/testcases/bin/$one_test .
}

function test_deploy {
    # set LTP_BUILD_DIR (possibly again), in case test_build was skipped
    LTP_BUILD_DIR="${WORKSPACE}/$(echo $JOB_BUILD_DIR | sed s/LTP_one_test/LTP/ | sed s/$TESTSPEC/default/)"

    local bdir="$BOARD_TESTDIR/fuego.$TESTDIR"
    local scenario=$FUNCTIONAL_LTP_ONE_TEST_SCENARIO
    if [ -z "$scenario" ] ; then
        put $one_test $bdir/
        # if the user hasn't specified any arguments,
        # warn user if there are default args
        if [ -z "$FUNCTIONAL_LTP_ONE_TEST_ARGS" ] ; then
            args=$(grep $one_test ${LTP_BUILD_DIR}/runtest/* | cut -d" " -f3- | head -n 1)
            if [ -n "$args" ] ; then
                echo "Warning: No arguments specified, but at least one scenario group for"
                echo "test '$one_test' has args of '${args}'"
                echo "Maybe add \"ARGS\":\"${args}\" in spec file?"
            fi
        fi
    else
        # copy helper files, runltp, ltp-pan, scenario file and the
        # test program to the board
        cmd "mkdir -p $bdir/bin $bdir/runtest $bdir/testcases/bin"
        put ${LTP_BUILD_DIR}/target_bin/IDcheck.sh $bdir/
        put ${LTP_BUILD_DIR}/target_bin/ver_linux $bdir/
        put ${LTP_BUILD_DIR}/target_bin/Version $bdir/
        put ${LTP_BUILD_DIR}/target_bin/runltp $bdir/
        put ${LTP_BUILD_DIR}/target_bin/bin/ltp-pan $bdir/bin/
        put ${LTP_BUILD_DIR}/target_bin/runtest/$scenario $bdir/runtest/
        put $one_test $bdir/testcases/bin/
    fi
}

function test_run {
    local bdir="$BOARD_TESTDIR/fuego.$TESTDIR"
    local scenario=$FUNCTIONAL_LTP_ONE_TEST_SCENARIO

    if [ -z "$scenario" ] ; then
        report "cd $bdir; ./$one_test $FUNCTIONAL_LTP_ONE_TEST_ARGS"
    else
        report "cd $bdir; ./runltp -f $scenario -s $one_test"
    fi
}

function test_processing {
    return
}
