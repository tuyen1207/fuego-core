tarball=../rt-tests/rt-tests-v1.1.1.tar.gz

NEED_ROOT=1

function test_pre_check {
    assert_define FUNCTIONAL_PI_TESTS_PARAMS
}

function test_build {
    patch -p1 -N -s < $TEST_HOME/../rt-tests/0001-Add-scheduling-policies-for-old-kernels.patch
    make NUMA=0 pi_stress
}

function test_deploy {
    put pi_stress  $BOARD_TESTDIR/fuego.$TESTDIR/
}

function test_run {
    report "cd $BOARD_TESTDIR/fuego.$TESTDIR; ./pi_stress $FUNCTIONAL_PI_TESTS_PARAMS"
}

function test_processing {
    local RETURN_VALUE=0

    log_compare "$TESTDIR" "0" "^ERROR:.*" "n"
    if [ $? -ne 0 ]; then RETURN_VALUE=1; fi
    log_compare "$TESTDIR" "0" "is deadlocked!" "n"
    if [ $? -ne 0 ]; then RETURN_VALUE=1; fi
    log_compare "$TESTDIR" "1" "^Total inversion performed" "p"
    if [ $? -ne 0 ]; then RETURN_VALUE=1; fi

    return $RETURN_VALUE
}
