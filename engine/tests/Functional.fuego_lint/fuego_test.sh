function test_pre_check {
    # check for required programs on host
    which flake8 || abort_job "Missing flake8 which is required for test"
#    which pylint || abort_job "Missing pylint which is required for test"
}

function test_run {
    # these tests are on the host
    tmpfile=$(mktemp)

    echo "### Testing ftc with flake8 ###" >$tmpfile
    set +e
    flake8 --count $FUEGO_CORE/engine/scripts/ftc >>$tmpfile
    if [ "$?" == "0" ] ; then
        echo "ok 1 flake8 test of ftc" >>$tmpfile
    else
        echo "not ok 1 flake8 test of ftc" >>$tmpfile
    fi
    set -e

# disable the pylint test until I figure out to cleanly install pylint
# into the container
#    echo "### Testing ftc with pylint ###" >>$tmpfile
#    pylint $FUEGO_CORE/engine/scripts/ftc >>$tmpfile
#    if [ "$?" == "0" ] ; then
#        echo "ok 1 pylint test of ftc" >>$tmpfile
#    else
#        echo "not ok 1 pylint test of ftc" >>$tmpfile
#    fi

    # move this all to target, to use normal log retrieval
    # this is kind of awkward - we shuold have something like "report_local"
    put $tmpfile $tmpfile
    report "cat $tmpfile"
    cmd rm $tmpfile
    rm "$tmpfile"
    report_append "echo Test done"
}

function test_processing() {
    log_compare "$TESTDIR" "1" "^ok" "p"
}
