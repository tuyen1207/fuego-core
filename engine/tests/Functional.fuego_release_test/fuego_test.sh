#!/bin/bash

readonly fuego_release_dir=fuego-release

readonly fuego_repo="${FUNCTIONAL_FUEGO_RELEASE_TEST_FUEGO_REPO}"
readonly fuego_branch="${FUNCTIONAL_FUEGO_RELEASE_TEST_FUEGO_BRANCH}"
readonly fuego_core_repo="${FUNCTIONAL_FUEGO_RELEASE_TEST_FUEGO_CORE_REPO}"
readonly fuego_core_branch="${FUNCTIONAL_FUEGO_RELEASE_TEST_FUEGO_CORE_BRANCH}"

function test_build {
    if [ -d ${fuego_release_dir} ]; then
        rm -r ${fuego_release_dir}
    fi
    echo "Cloning fuego from ${fuego_repo}:${fuego_branch}"
    git clone --depth 1 --single-branch --branch "${fuego_branch}" \
        "${fuego_repo}" "${fuego_release_dir}/fuego"

    echo "Cloning fuego-core from ${fuego_core_repo}:${fuego_core_branch}"
    git clone --depth 1 --single-branch --branch "${fuego_core_branch}" \
        "${fuego_core_repo}" "${fuego_release_dir}/fuego-core"
    cd -
}

function test_run {
    sudo -n ${TEST_HOME}/test_run.py "${fuego_release_dir}/fuego"
    if [ "${?}" = 0 ]; then
        report "echo ok 1 fuego release test"
    else
        report "echo not ok 1 fuego release test"
    fi
}

function test_processing {
    log_compare "$TESTDIR" "1" "^ok" "p"
}
