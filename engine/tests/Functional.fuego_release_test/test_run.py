#!/usr/bin/env python3
import argparse
import atexit
import http
import http.client
import logging
import os
import re
import subprocess
import sys
import time

import docker
import pexpect
import requests
from selenium import webdriver
from selenium.webdriver.common.by import By
import selenium.common.exceptions as selenium_exceptions

LOGGER = logging.getLogger('test_run')
STREAM_HANDLER = logging.StreamHandler()
STREAM_HANDLER.setFormatter(
    logging.Formatter("%(name)s:%(levelname)s: %(message)s"))
LOGGER.setLevel(logging.DEBUG)
LOGGER.addHandler(STREAM_HANDLER)


def loop_until_timeout(func, timeout=10, num_tries=5):
    LOGGER.debug("Running %s()", func.__name__)

    for _ in range(num_tries):
        LOGGER.debug("  Try number %s...", _ + 1)
        if func():
            LOGGER.debug("  Success")
            return True
        time.sleep(timeout/num_tries)
    LOGGER.debug("  Failure")

    return False


class SeleniumCommand:
    def exec(self, selenium_ctx):
        self.driver = selenium_ctx.driver
        self.driver.refresh()
        LOGGER.debug("Executing Selenium Command '%s'",
                     self.__class__.__name__)

    @staticmethod
    def check_element_text(element, text):
        try:
            if text in element.text:
                LOGGER.\
                    debug("  Text '%s' matches element.text '%s'",
                          text, element.text)
                return True
            else:
                LOGGER.\
                    debug("  Text '%s' does not match element.text '%s'",
                          text, element.text)
                return False
        except (selenium_exceptions.ElementNotVisibleException,
                selenium_exceptions.NoSuchAttributeException,):
            LOGGER.error("  Element has no visible Text")
            return False

    @staticmethod
    def click_element(element):
        try:
            element.click()
            LOGGER.debug("  Element clicked")
            return True
        except (selenium_exceptions.ElementClickInterceptedException,
                selenium_exceptions.ElementNotVisibleException,):
            LOGGER.error("  Element is not clickable")
            return False

    @staticmethod
    def find_element(context, locator, pattern):
        try:
            element = context.driver.find_element(locator, pattern)
        except selenium_exceptions.NoSuchElementException:
            LOGGER.error("  Element not found")
            return None

        LOGGER.debug("  Element found")
        return element


class Visit(SeleniumCommand):
    def __init__(self, url, timeout=10, expected_result=200):
        self.url = url
        self.timeout = timeout
        self.expected_result = expected_result

    def exec(self, selenium_ctx):
        super().exec(selenium_ctx)

        LOGGER.debug("  Visiting '%s'", self.url)
        self.driver.get(self.url)

        r = requests.get(self.url)
        if r.status_code != self.expected_result:
            LOGGER.debug("  HTTP Status Code '%s' is different " +
                         "from the expected '%s'", r.status_cod, self.url)
            return False

        LOGGER.debug("  HTTP Status Code is same as expected '%s'",
                     r.status_code)
        return True


class CheckText(SeleniumCommand):
    def __init__(self, locator, pattern, text='', expected_result=True):
        self.pattern = pattern
        self.locator = locator
        self.text = text
        self.expected_result = expected_result

    def exec(self, selenium_ctx):
        super().exec(selenium_ctx)

        element = SeleniumCommand.\
            find_element(selenium_ctx, self.locator, self.pattern)
        if element:
            result = SeleniumCommand.check_element_text(element, self.text)

        return result == self.expected_result


class Click(SeleniumCommand):
    def __init__(self, locator, pattern):
        self.pattern = pattern
        self.locator = locator

    def exec(self, selenium_ctx):
        super().exec(selenium_ctx)

        element = SeleniumCommand.\
            find_element(selenium_ctx, self.locator, self.pattern)
        if element:
            return SeleniumCommand.click_element(element)

        return False


class Back(SeleniumCommand):
    def exec(self, selenium_ctx):
        super().exec(selenium_ctx)

        LOGGER.debug("  Going back")
        self.driver.back()

        return True


class ShExpect():
    BASH_PATTERN = 'test_run_pr1:#'
    COMMAND_RESULT_PATTERN = re.compile('^([0-9]+)', re.M)
    OUTPUT_VARIABLE = 'cmd_output'
    COMMAND_OUTPUT_DELIM = ':test_run_cmd_out:'
    COMMAND_OUTPUT_PATTERN = re.compile(
        r'^%s(.*)%s\s+%s' %
        (COMMAND_OUTPUT_DELIM, COMMAND_OUTPUT_DELIM, BASH_PATTERN),
        re.M | re.S)

    def __init__(self, cmd, expected_output=None, expected_result=0):
        self.cmd = cmd
        self.expected_result = expected_result
        self.expected_output = expected_output

    def exec(self, pexpect_ctx):
        self.client = pexpect_ctx.client

        LOGGER.debug("Executing command '%s'", self.cmd)
        try:
            self.client.sendline('%s=$(%s 2>&1)' %
                                 (self.OUTPUT_VARIABLE, self.cmd))
            self.client.expect(self.BASH_PATTERN)

            self.client.sendline('echo $?')
            self.client.expect(self.COMMAND_RESULT_PATTERN)
            result = int(self.client.match.group(1))

            self.client.sendline(
                'echo "%s${%s}%s"' % (self.COMMAND_OUTPUT_DELIM,
                                        self.OUTPUT_VARIABLE,
                                        self.COMMAND_OUTPUT_DELIM))
            self.client.expect(self.COMMAND_OUTPUT_PATTERN)
            out = self.client.match.group(1)

            if result != self.expected_result:
                LOGGER.error("The command '%s' returned the code '%d', " +
                             "but the expected code is '%d'" +
                             "\nCommand output: '%s'",
                             self.cmd, result, self.expected_result, out)
                return False
            if self.expected_output is not None and \
                    re.search(self.expected_output, out) is None:
                LOGGER.error("Wrong output for command '%s'. " +
                             "Expected '%s'\nReceived '%s'",
                             self.cmd, self.expected_output, out)
                return False
        except pexpect.exceptions.TIMEOUT:
            LOGGER.error("Timeout for command '%s'", self.cmd)
            return False
        except pexpect.exceptions.EOF:
            LOGGER.error("Lost connection with docker. Aborting")
            return False
        return True


class FuegoContainer:
    def __init__(self, install_script, image_name, container_name,
                 jenkins_port, rm_after_test=True):
        self.install_script = install_script
        self.image_name = image_name
        self.container_name = container_name
        self.jenkins_port = jenkins_port
        self.rm_after_test = rm_after_test

    def delete(self):
        if self.container:
            if self.rm_after_test:
                LOGGER.debug("Removing Container")
                self.container.remove(force=True)
            else:
                LOGGER.debug("Not Removing the test container")

    def install(self):
        self.docker_client = docker.APIClient()
        self.container = self.setup_docker()

        return self.container

    def stop(self):
        self.container.remove(force=True)
        self.container = None

    def setup_docker(self):
        def this_container_id():
            with open('/proc/self/cgroup', 'rt') as f:
                f_text = f.read()

                if 'docker' not in f_text:
                    return None

                for c in self.docker_client.containers(quiet=True):
                    if c['Id'] in f_text:
                        return c['Id']

                return None

        def map_to_host(mounts, container_id):
            host_mounts = self.docker_client.\
                inspect_container(container_id)['Mounts']

            for mount in mounts:
                LOGGER.debug("  Trying to find '%s' mountpoint in the host",
                             mount['source'])
                for host_mount in host_mounts:
                    if mount['source'].startswith(host_mount['Destination']):
                        mount['source'] = mount['source'].\
                            replace(host_mount['Destination'],
                                    host_mount['Source'], 1)
                        LOGGER.debug("    Found: '%s'", mount['source'])
                        break
                else:
                    LOGGER.debug("    Not Found")
                    mount['source'] = None

        cmd = './%s %s' % (self.install_script, self.image_name)
        LOGGER.debug("Running '%s' to install the docker image. " +
                     "This may take a while....", cmd)
        status = subprocess.call(cmd, shell=True)

        LOGGER.debug("Install output code: %s", status)

        if status != 0:
            return None

        docker_client = docker.from_env()
        containers = docker_client.containers.list(
            all=True, filters={'name': self.container_name})
        if containers:
            LOGGER.debug(
                "Erasing the container '%s', so a new one can be created",
                self.container_name)
            containers[0].remove(force=True)

        mounts = [
            {'source': os.path.abspath('./fuego-rw'),
             'destination':  '/fuego-rw',
             'readonly': False,
             },
            {'source': os.path.abspath('./fuego-ro'),
             'destination':  '/fuego-ro',
             'readonly': True,
             },
            {'source': os.path.abspath('../fuego-core'),
             'destination':  '/fuego-core',
             'readonly': True,
             },
        ]

        our_id = this_container_id()

        if our_id:
            LOGGER.debug("Running inside the Docker container %s", our_id)
            map_to_host(mounts, our_id)
        else:
            LOGGER.debug("Not running inside a Docker container")

        LOGGER.debug("Creating container with the following mountpoints:")
        LOGGER.debug("  %s", mounts)

        container = docker_client.containers.create(
            self.image_name,
            stdin_open=True, tty=True, network_mode='bridge',
            mounts=[docker.types.Mount(m['destination'],
                                       m['source'],
                                       type='bind',
                                       read_only=m['readonly'])
                    for m in mounts],
            name=self.container_name, command='/bin/bash')

        LOGGER.debug("Container '%s' created", self.container_name)
        return container

    def is_running(self):
        try:
            container_status = self.docker_client.\
                inspect_container(self.container_name)['State']['Running']
        except KeyError:
            return False

        return container_status

    def get_ip(self):
        container_addr = None

        if not self.is_running():
            return None

        def fetch_ip():
            nonlocal container_addr
            try:
                container_addr = self.docker_client.\
                    inspect_container(
                        self.container_name)['NetworkSettings']['IPAddress']
            except KeyError:
                return False

            if container_addr is None:
                return False
            else:
                return True

        if not loop_until_timeout(fetch_ip, timeout=10):
            LOGGER.error("Could not fetch the container IP address")
            return None

        return container_addr

    def get_url(self):
        container_addr = self.get_ip()

        if container_addr:
            return 'http://%s:%s/fuego/' % (container_addr, self.jenkins_port)
        else:
            return None


class PexpectContainerSession():
    def __init__(self, container, start_script, timeout):
        self.container = container
        self.start_script = start_script
        self.timeout = timeout

    def start(self):
        LOGGER.debug(
            "Starting container '%s'", self.container.container_name)
        self.client = pexpect.spawnu(
            '%s %s' % (self.start_script, self.container.container_name),
            echo=False, timeout=self.timeout)

        PexpectContainerSession.set_ps1(self.client)

        if not self.wait_for_jenkins():
            return False

        return True

    def __del__(self):
        self.client.terminate(force=True)

    @staticmethod
    def set_ps1(client):
        client.sendline('export PS1="%s"' % ShExpect.BASH_PATTERN)
        client.expect(ShExpect.BASH_PATTERN)

    def wait_for_jenkins(self):
        def ping_jenkins():
            try:
                conn = http.client.HTTPConnection(container_addr,
                                                  self.container.jenkins_port,
                                                  timeout=30)
                conn.request('HEAD', '/fuego/')
                resp = conn.getresponse()
                version = resp.getheader('X-Jenkins')
                status = resp.status
                conn.close()
                LOGGER.debug(
                    "  HTTP Response code: '%d' - Jenkins Version: '%s'",
                    status, version)
                if status == http.client.OK and version is not None:
                    return True
            except (ConnectionRefusedError, OSError):
                return False

            return False

        container_addr = self.container.get_ip()
        if container_addr is None:
            return False
        LOGGER.debug("Trying to reach jenkins at container '%s' via " +
                     "the container's IP '%s' at port '%d'",
                     self.container.container_name,
                     container_addr, self.container.jenkins_port)
        if not loop_until_timeout(ping_jenkins, timeout=60):
            LOGGER.error("Could not connect to jenkins")
            return False

        return True


class SeleniumContainerSession():
    def __init__(self, container):
        self.container = container
        self.driver = None
        self.root_url = container.get_url()

    def start(self):
        options = webdriver.ChromeOptions()
        options.add_argument('headless')
        options.add_argument('no-sandbox')
        options.add_argument('window-size=1200x600')
        options.add_experimental_option(
            'prefs', {'intl.accept_languages': 'en,en_US'})

        self.driver = webdriver.Chrome(chrome_options=options)
        self.driver.implicitly_wait(3)

        self.driver.get(self.root_url)

        LOGGER.debug("Started a Selenium Session on %s", self.root_url)
        return True

    def __del__(self):
        if self.driver:
            self.driver.quit()


def main():
    DEFAULT_TIMEOUT = 120
    DEFAULT_IMAGE_NAME = 'fuego-release'
    DEFAULT_CONTAINER_NAME = 'fuego-release-container'
    DEFAULT_INSTALL_SCRIPT = 'install.sh'
    DEFAULT_START_SCRIPT = 'fuego-host-scripts/docker-start-container.sh'
    DEFAULT_JENKINS_PORT = 8080

    @atexit.register
    def cleanup():
        if container:
            container.delete()

    def execute_tests(timeout):
        LOGGER.debug("Starting tests")

        ctx_mapper = {
            ShExpect: pexpect_session,
            SeleniumCommand: selenium_session
        }

        tests_ok = True
        for cmd in COMMANDS_TO_TEST:
            for base_class, ctx in ctx_mapper.items():
                if isinstance(cmd, base_class):
                    if not cmd.exec(ctx):
                        tests_ok = False
                        break

        if tests_ok:
            LOGGER.debug("Tests finished.")

        return tests_ok

    parser = argparse.ArgumentParser()
    parser.add_argument('working_dir', help="The working directory", type=str)
    parser.add_argument('-s', '--install-script',
                        help="The script that will be used to install the " +
                        "docker image. Defaults to '%s'" %
                        DEFAULT_INSTALL_SCRIPT, default=DEFAULT_INSTALL_SCRIPT,
                        type=str)
    parser.add_argument('-a', '--start-script',
                        help="The script used to start the container. " +
                        "Defaults to '%s'" % DEFAULT_START_SCRIPT,
                        default=DEFAULT_START_SCRIPT,
                        type=str)
    parser.add_argument('-i', '--image-name', default=DEFAULT_IMAGE_NAME,
                        help="The image name that should be used. " +
                        "Defaults to '%s'" % DEFAULT_IMAGE_NAME, type=str)
    parser.add_argument('-c', '--container-name',
                        default=DEFAULT_CONTAINER_NAME,
                        help="The container name that should be used for " +
                        "the test. Defaults to '%s'" % DEFAULT_CONTAINER_NAME,
                        type=str)
    parser.add_argument('-t', '--timeout', help="The timeout value for " +
                        "commands. Defaults to '%s'" % DEFAULT_TIMEOUT,
                        default=DEFAULT_TIMEOUT, type=int)
    parser.add_argument('-j', '--jenkins-port',
                        help="The port where the jenkins is running on the " +
                        "test container. Defaults to '%s'" %
                        DEFAULT_JENKINS_PORT, default=DEFAULT_JENKINS_PORT,
                        type=int)
    parser.add_argument('--no-rm-container',
                        help="Do not remove the container after tests are " +
                        "complete.",
                        dest='rm_test_container',
                        default=True, action='store_false')
    args = parser.parse_args()

    LOGGER.debug("Changing working dir to '%s'", args.working_dir)
    os.chdir(args.working_dir)

    container = FuegoContainer(args.install_script, args.image_name,
                               args.container_name, args.jenkins_port,
                               rm_after_test=args.rm_test_container)
    if not container.install():
        return 1

    pexpect_session = PexpectContainerSession(container, args.start_script,
                                              args.timeout)
    if not pexpect_session.start():
        return 1

    selenium_session = SeleniumContainerSession(container)
    if not selenium_session.start():
        return 1

    COMMANDS_TO_TEST = [
        # Set Selenium Browser root
        Visit(url=container.get_url()),

        # Add Nodes
        ShExpect('ftc add-nodes docker'),
        ShExpect('ftc list-nodes -q', r'.*docker.*'),
        CheckText(By.ID, 'executors', text='master'),
        CheckText(By.ID, 'executors', text='docker'),

        # Add Fuego TestPlan
        ShExpect('ftc add-jobs -b docker -p testplan_fuego_tests'),
        ShExpect('ftc list-jobs', r'.*docker\.testplan_fuego_tests\.batch.*'),

        Click(By.PARTIAL_LINK_TEXT, 'docker'),
        CheckText(By.ID, 'projectstatus',
                  text='docker.testplan_fuego_tests.batch'),
        Back(),

        # Install Views
        ShExpect('ftc add-view batch .*.batch'),
        CheckText(By.ID, 'projectstatus-tabBar',
                  text='batch'),

        # Start a Test through Command Line
        ShExpect('ftc build-jobs *.*.Functional.fuego_board_check'),
        CheckText(By.ID, 'buildQueue',
                  text='Functional.fuego_board_check'),

        # Start a Test through Jenkins UI (Xpath)
        Click(By.XPATH,
              '//img[@title="Schedule a build for ' +
              'docker.default.Functional.hello_world"]'),
        CheckText(By.ID, 'executors',
                  text='docker.default.Functional.hello_world'),
    ]

    if not execute_tests(args.timeout):
        return 1

    return 0


if __name__ == '__main__':
    sys.exit(main())
