#!/usr/bin/python
# See common.py for description of command-line arguments

import os, sys

sys.path.insert(0, os.environ['FUEGO_CORE'] + '/engine/scripts/parser')
import common as plib

measurements = {}

regex_string = '.*\(High:(\d+) Medium:(\d+) Low:(\d+) \?:(\d+)\)'
matches = plib.parse_log(regex_string)

if matches:
    measurements['default.HIGH']    = [{"name": "Count", "measure" : float(matches[0][0])}]
    measurements['default.MEDIUM']  = [{"name": "Count", "measure" : float(matches[0][1])}]
    measurements['default.LOW']     = [{"name": "Count", "measure" : float(matches[0][2])}]
    measurements['default.UNKNOWN'] = [{"name": "Count", "measure" : float(matches[0][3])}]

sys.exit(plib.process(measurements))
