tarball=../rt-tests/rt-tests-v1.1.1.tar.gz

NEED_ROOT=1
TEST_COMMAND="rt-migrate-test"

function test_pre_check {
    assert_define BENCHMARK_RT_MIGRATE_TEST_PARAMS
}

function test_build {
    patch -p1 -N -s < $TEST_HOME/../rt-tests/0001-Add-scheduling-policies-for-old-kernels.patch
    make NUMA=0 ${TEST_COMMAND}
}

function test_deploy {
    put ${TEST_COMMAND} $BOARD_TESTDIR/fuego.$TESTDIR/
}

function test_run {
    report "cd $BOARD_TESTDIR/fuego.$TESTDIR; ./${TEST_COMMAND} $BENCHMARK_RT_MIGRATE_TEST_PARAMS"
}
