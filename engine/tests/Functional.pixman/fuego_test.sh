tarball=pixman-0.32.8.tar.gz

function test_build {
    libtoolize --automake
    aclocal --system-acdir=${SDKROOT}usr/share/aclocal
    autoreconf --verbose --install --force --exclude=autopoint
    autoconf
    autoheader
    automake -a
    ./configure --host=$PREFIX
    make
    echo "#!/bin/bash
    if ./a1-trap-test ; then
    echo 'TEST-1 OK'
    else
    echo 'TEST-1 FAILED'
    fi

    if ./alpha-loop ; then
    echo 'TEST-2 OK'
    else
    echo 'TEST-2 FAILED'
    fi

    if ./alphamap ; then
    echo 'TEST-3 OK'
    else
    echo 'TEST-3 FAILED'
    fi

    if ./affine-test ; then
    echo 'TEST-4 OK'
    else
    echo 'TEST-4 FAILED'
    fi

    if ./blitters-test ; then
    echo 'TEST-5 OK'
    else
    echo 'TEST-5 FAILED'
    fi

    if ./composite-traps-test ; then
    echo 'TEST-6 OK'
    else
    echo 'TEST-6 FAILED'
    fi

    if ./glyph-test ; then
    echo 'TEST-7 OK'
    else
    echo 'TEST-7 FAILED'
    fi

    if ./matrix-test ; then
    echo 'TEST-8 OK'
    else
    echo 'TEST-8 FAILED'
    fi

    if ./rotate-test ; then
    echo 'TEST-9 OK'
    else
    echo 'TEST-9 FAILED'
    fi

    if ./check-formats xor x8r8g8b8 x2r10g10b10 ; then
    echo 'TEST-10 OK'
    else
    echo 'TEST-10 FAILED'
    fi

    if ./pixel-test ; then
    echo 'TEST-11 OK'
    else
    echo 'TEST-11 FAILED'
    fi

    if ./prng-test ; then
    echo 'TEST-12 OK'
    else
    echo 'TEST-12 FAILED'
    fi

    if ./combiner-test ; then
    echo 'TEST-13 OK'
    else
    echo 'TEST-13 FAILED'
    fi

    if ./composite ; then
    echo 'TEST-14 OK'
    else
    echo 'TEST-14 FAILED'
    fi

    if ./oob-test ; then
    echo 'TEST-15 OK'
    else
    echo 'TEST-15 FAILED'
    fi

    if ./pdf-op-test ; then
    echo 'TEST-16 OK'
    else
    echo 'TEST-16 FAILED'
    fi

    if ./fetch-test ; then
    echo 'TEST-17 OK'
    else
    echo 'TEST-17 FAILED'
    fi

    if ./radial-perf-test ; then
    echo 'TEST-18 OK'
    else
    echo 'TEST-18 FAILED'
    fi

    if ./gradient-crash-test ; then
    echo 'TEST-19 OK'
    else
    echo 'TEST-19 FAILED'
    fi

    if ./region-contains-test ; then
    echo 'TEST-20 OK'
    else
    echo 'TEST-20 FAILED'
    fi

    if ./region-test ; then
    echo 'TEST-21 OK'
    else
    echo 'TEST-21 FAILED'
    fi

    if ./region-translate-test ; then
    echo 'TEST-22 OK'
    else
    echo 'TEST-22 FAILED'
    fi

    if ./scaling-crash-test ; then
    echo 'TEST-23 OK'
    else
    echo 'TEST-23 FAILED'
    fi

    if ./scaling-helpers-test ; then
    echo 'TEST-24 OK'
    else
    echo 'TEST-24 FAILED'
    fi

    if ./scaling-test ; then
    echo 'TEST-25 OK'
    else
    echo 'TEST-25 FAILED'
    fi

    if ./thread-test ; then
    echo 'TEST-26 OK'
    else
    echo 'TEST-26 FAILED'
    fi

    if ./stress-test ; then
    echo 'TEST-27 OK'
    else
    echo 'TEST-27 FAILED'
    fi

    if ./trap-crasher ; then
    echo 'TEST-28 OK'
    else
    echo 'TEST-28 FAILED'
    fi

    if ./infinite-loop ; then
    echo 'TEST-29 OK'
    else
    echo 'TEST-29 FAILED'
    fi

    if ./lowlevel-blt-bench -b -c add_n_8888 ; then
    echo 'TEST-30 OK'
    else
    echo 'TEST-30 FAILED'
    fi
    " > run-tests.sh
}

function test_deploy {
    put test/.libs/* run-tests.sh $BOARD_TESTDIR/fuego.$TESTDIR/
}

function test_run {
    report "cd $BOARD_TESTDIR/fuego.$TESTDIR;ls; sh -v run-tests.sh 2>&1"
}

function test_processing {
    log_compare "$TESTDIR" "30" "^TEST.*OK" "p"
    log_compare "$TESTDIR" "0" "^TEST.*FAILED" "n"
}


