# some functions are shared between Benchmark.OpenSSL and Functional.OpenSSL
tarball=../OpenSSL/openssl-1.0.0t.tar.gz
source $FUEGO_CORE/engine/tests/OpenSSL/openssl.sh

function test_deploy {
    put apps $BOARD_TESTDIR/fuego.$TESTDIR/
}

function test_run {
    report "cd $BOARD_TESTDIR/fuego.$TESTDIR; apps/openssl speed"
}


