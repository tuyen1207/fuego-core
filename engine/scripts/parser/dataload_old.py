#!/usr/bin/python
#
# Copyright (c) 2014 Cogent Embedded, Inc.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

"""
This module parses plot_data and generates a json file that is easy to
use by the flot plugin.
"""

import sys, os, re
import simplejson as json

FUEGO_CORE=os.environ['FUEGO_CORE']
FUEGO_RW=os.environ['FUEGO_RW']
TESTDIR=os.environ['TESTDIR']
PLOTDATA_FILE = '%s/logs/%s/plot.data' % (FUEGO_RW, TESTDIR)
RESULTS_FILE = '%s/logs/%s/results.json' % (FUEGO_RW, TESTDIR)

with open(PLOTDATA_FILE) as f:
    plot_data = f.readlines()

plot_data_json = {}
for line in plot_data:
    # split the line into meaningful values
    # NODE_NAME TESTDIR TESTSPEC BUILD_NUMBER BUILD_ID BUILD_TIMESTAMP FWVER PLATFORM SECTION REF.VALUE CUR.VALUE
    line_split = line.split()
    ITEM_NODE_NAME       = line_split[0]
    ITEM_TESTDIR         = line_split[1]
    ITEM_TESTSPEC        = line_split[2]
    ITEM_BUILD_NUMBER    = line_split[3]
    ITEM_BUILD_ID        = line_split[4]
    ITEM_BUILD_TIMESTAMP = line_split[5]
    ITEM_FWVER           = line_split[6]
    ITEM_PLATFORM        = line_split[7]
    ITEM_SECTION         = line_split[8]
    ITEM_REF_VALUE       = line_split[9]
    ITEM_CUR_VALUE       = line_split[10]

    # section can be 'groupname.test' or 'test'.
    if '.' in ITEM_SECTION:
        groupname, test = ITEM_SECTION.split('.')
    else:
        groupname, test = ITEM_SECTION, ITEM_SECTION

    # create a unique key
    key = '%s-%s-%s-%s-%s' % (ITEM_NODE_NAME, ITEM_TESTSPEC, ITEM_FWVER, groupname, test)
    if not plot_data_json.get(key):
        plot_data_json[key] = {
            'data' : [ITEM_CUR_VALUE],
            'ref'  : [ITEM_REF_VALUE],
            'timestamp' : [ITEM_BUILD_TIMESTAMP],
            'board' : ITEM_NODE_NAME,
            'spec' : ITEM_TESTSPEC,
            'fwver' : ITEM_FWVER,
            'platform' : ITEM_PLATFORM,
            'groupname' : groupname,
            'test' : test }
    else:
        plot_data_json[key]['data'].append(ITEM_CUR_VALUE)
        plot_data_json[key]['ref'].append(ITEM_REF_VALUE)
        plot_data_json[key]['timestamp'].append(ITEM_BUILD_TIMESTAMP)

print "Writing results to ", RESULTS_FILE
with open(RESULTS_FILE, 'w') as f:
    f.write(json.dumps(plot_data_json))
